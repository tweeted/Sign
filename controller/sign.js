var crypto = require('crypto');
var md5 = require('md5');
var sd = require('silly-datetime');

//param 请求参数 appsecret 加密串 signmethod 加密类型MD5
function sign(param, appsecret, signmethod) {
    if (signmethod != 'MD5') {
        return '';
    }
    var lists = [];
    var param_str = appsecret;
    for (item in param) {
        lists.push(item);
    }
    lists.sort();
    lists.forEach(function (key) {
        param_str += key + param[key];
    });
    param_str += appsecret;
    return genMd5(param_str);
}

function genMd5(text) {
    return crypto.createHash('md5').update(text, 'utf-8').digest('hex');
}

// dumovic  param 请求参数 appsecret 加密串 signmethod 加密类型MD5
function du_sign(param, appsecret, signmethod) {
    if (signmethod != 'MD5') {
        return '';
    }
    var lists = [];
    var param_str = ''
    for (item in param) {
        lists.push(item);
    }
    lists.sort();
    lists.forEach(function (key) {
        param_str += key + param[key];
    });
    param_str += appsecret;
    console.log(param_str);

    var str = md5(param_str);
    console.log(str.toLocaleUpperCase());

    return str.toLocaleUpperCase();
}



exports.sign_365 = function (req, res) {
    var apikey = 'b421b9a21075a359c09a36db79325d5b';
    var secretkey = 'd3243685d9922998ddd4232f38b53555';
    //获取对应格式时间戳
    var time = sd.format(new Date(), 'YYYY/MM/DD HH:mm:ss');
    console.log(time);

    var sign = 'apikey' + apikey + 'secretkey' + secretkey + "timestamp" + time
    console.log(sign);
    var result = {
        Sign: md5(sign),
        Timestamp: time,
        ApiKey: 'b421b9a21075a359c09a36db79325d5b'
    }

    res.send(result)
}


exports.getsign = function (req, res) {

    res.send(sign(req.query, '4e0d1be81540c4d1f0868d329b3229467f071f6c', "MD5"))
}

exports.get_du_sign = function (req, res) {
    res.send(du_sign(req.query, '9DD3F51A1B333727EA55DC0AF0303863', "MD5"))
}

var qunar_token = '80b033a1987c18a36e9db52482b58b09'
var qunar_key = '3e9d4a51d289093d1ff91d68590a4e3c'
//去哪儿
exports.get_qunar_sign = function (req, res) {
    tag = req.query.tag
    //params是请求参数的json
    params = JSON.parse(req.query.params)

    var params_str = JSON.stringify(params)
    console.log('params_str =  ' + params_str);

    var createTime = new Date().getTime();

    console.log(' createTime  = ' + createTime);
    console.log(params_str.toString())

    var sing_str = "createTime=" + createTime + "key=" + qunar_key + "params=" + params_str.toString() + "tag=" + tag + "token=" + qunar_token
    console.log('sing_str = ' + sing_str);

    var str = md5(sing_str);
    console.log('str.toLocaleLowerCase() = ' + str.toLocaleLowerCase());

    var sign = {
        sign: str.toLocaleLowerCase(),
        createTime: createTime,
        params: params_str,
        token: qunar_token,
        tag: tag
    }
    res.send(sign)
}
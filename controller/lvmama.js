var md5 = require('md5');
var sd = require('silly-datetime');
var rp = require('request-promise')

// var url = 'https://union.lvmama.com/tnt_cps/cps/newRedirect2.do?source=53198&lvmamakey=4dzn&isNew=new'

// console.log(encodeURI(url));
// console.log(encodeURIComponent(url));
// var timestamp =new Date().getTime();
// console.log(timestamp);
var userName = 'cps001@53198';
var shop_key = '53198_centaurs'
// console.log( md5(userName + shop_key + timestamp));
var api = 'http://api.lvmama.com'
var appkey = 'cps1562275ZFEG';
var secret = '5a37295201cc445b';

// console.log('sign=' + sign);
// var endtime=sd.format(new Date(), 'YYYYMMDD');
// console.log(endtime);

exports.ticketsProductList = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + appkey + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()
  page = req.query.page

  var url = api + '/distributorApi/1.0/cpsplatformapi/ticket/TicketsProductList' + '?page=' + page + '&appKey=' + appkey + '&timestamp=' + timestamp +
    '&messageFormat=xml&sign=' + bigSign

  var options = {
    url: url,
    json: true
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })
}

exports.ticketsProduct = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + appkey + timestamp + secret)


  var bigSign = sign.toLocaleUpperCase()
  productId = req.query.productId

  var url = api + '/distributorApi/1.0/cpsplatformapi/ticket/TicketsProduct?productId=' + productId + "&appKey=" + appkey + '&timestamp=' + timestamp +
    '&messageFormat=xml&sign=' + bigSign
  var options = {
    url: url,
    json: true
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })

}

exports.lineProductList = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + appkey + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()

  page = req.query.page

  var url = api + '/distributorApi/1.0/cpsplatformapi/route/LineProductList?page=' + page + '&appKey=' + appkey + '&timestamp=' + timestamp +
    '&messageFormat=xml&sign=' + bigSign

  var options = {
    url: url,
    json: true
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })

}

exports.lineProduct = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + appkey + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()

  productId = req.query.productId

  var url = api + '/distributorApi/1.0/cpsplatformapi/route/LineProduct?productId=' + productId + "&appKey=" + appkey + '&timestamp=' + timestamp +
    '&messageFormat=xml&sign=' + bigSign
  var options = {
    url: url,
    json: true
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })

}

exports.scenicInfoListByPage = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()

  currentPage = req.query.currentPage

  var url = api + '/distributorApi/2.0/api/ticketProd/scenicInfoListByPage?currentPage=' + currentPage + "&appKey=" + appkey + '&timestamp=' + timestamp +
    '&messageFormat=json&sign=' + sign
  var options = {
    url: url,
    json: true,
    method: 'POST'
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })
}


exports.productInfoListByPage = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()

  currentPage = req.query.currentPage

  var url = api + '/distributorApi/2.0/api/ticketProd/scenicInfoListByPage?currentPage=' + currentPage + "&appKey=" + appkey + '&timestamp=' + timestamp +
    '&messageFormat=json&sign=' + sign
  var options = {
    url: url,
    json: true,
    method: 'POST'
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })
}

exports.productPriceListByPage = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()

  var currentPage = req.query.currentPage
  var beginDate = req.query.beginDate
  var endDate = req.query.endDate

  var url = api + '/distributorApi/2.0/api/ticketProd/productPriceListByPage?currentPage=' + currentPage + "&appKey=" + appkey + '&timestamp=' + timestamp +
    '&messageFormat=json&sign=' + sign
  url = beginDate ? url + "&beginDate=" + beginDate : url
  url = endDate ? url + "&endDate=" + endDate : url
  var options = {
    url: url,
    json: true,
    method: 'POST'
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })
}

exports.productInfoList = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()

  var productIds = req.query.productIds

  var url = api + '/distributorApi/2.0/api/ticketProd/productInfoList?productIds=' + productIds + "&appKey=" + appkey + '&timestamp=' + timestamp +
    '&messageFormat=json&sign=' + sign

  var options = {
    url: url,
    json: true,
    method: 'POST'
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })
}

exports.goodInfoList = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()

  var goodsIds = req.query.goodsIds

  var url = api + '/distributorApi/2.0/api/ticketProd/goodInfoList?goodsIds=' + goodsIds + "&appKey=" + appkey + '&timestamp=' + timestamp +
    '&messageFormat=json&sign=' + sign

  var options = {
    url: url,
    json: true,
    method: 'POST'
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })
}

exports.goodPriceList = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()

  var goodsIds = req.query.goodsIds
  var beginDate = req.query.beginDate
  var endDate = req.query.endDate

  var url = api + '/distributorApi/2.0/api/ticketProd/goodInfoList?goodsIds=' + goodsIds + "&appKey=" + appkey + '&timestamp=' + timestamp +
    '&messageFormat=json&sign=' + sign
  url = beginDate ? url + "&endDate=" + endDate : url
  url = endDate ? url + "&endDate=" + endDate : url

  var options = {
    url: url,
    json: true,
    method: 'POST'
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })
}


exports.scenicInfoList = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()

  var scenicId = req.query.scenicId

  var url = api + '/distributorApi/2.0/api/ticketProd/goodInfoList?scenicId=' + scenicId + "&appKey=" + appkey + '&timestamp=' + timestamp +
    '&messageFormat=json&sign=' + sign

  var options = {
    url: url,
    json: true,
    method: 'POST'
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })
}

exports.searchSeasonTicket = function (req, res) {
  var timestamp = new Date().getTime();
  var sign = md5(secret + timestamp + secret)
  var bigSign = sign.toLocaleUpperCase()

  var productId = req.query.productId
  var goodsId = req.query.goodsId

  var url = api + '/distributorApi/2.0/api/ticketProd/goodInfoList?productId=' + productId + "&appKey=" + appkey + '&timestamp=' + timestamp +
    '&messageFormat=json&sign=' + sign + '&goodsId=' + goodsId

  var options = {
    url: url,
    json: true,
    method: 'POST'
  }

  rp(options).then(function (result) {
    console.log(result);
    res.send(result);
  })
}
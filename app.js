/**
 * @copyright Centaurs Technologies Co. 2018
 * @author zzh
 */

var express = require('express')
var app = express()
var bodyParser = require('body-parser')
//var plugin = require("centaurs-test-plugin")

var port = 41003
app.set("port", port);

app.set('views', __dirname + '/views')
app.set('view_options', {
  layout: false
})

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: false
}))
// parse application/json
app.use(bodyParser.json())

var sign = require("./controller/sign.js")
var utiles = require('./controller/utils.js')
var lvmama = require('./controller/lvmama')
var lanxingman = require('./controller/lanxingman')
var bills = require('./controller/bills.js')

app.get('/sign', sign.getsign)
app.get('/du/sign', sign.get_du_sign)
app.get('/365/sign', sign.sign_365)
app.get('/qunar/sign', sign.get_qunar_sign)

app.get('/utils/timestamp', utiles.get_timestamp)

app.get('/lanxingman/constellation',lanxingman.constellation)

//驴妈妈门票批量产品信息接口 ticketsProductList
app.get('/lvmama/tickets/product/list', lvmama.ticketsProductList)
//门票单个产品信息接口 ticketsProduct
app.get('/lvmama/tickets/product', lvmama.ticketsProduct)
//线路批量产品信息接口 lineProductList
app.get('/lvmama/line/product/list', lvmama.lineProductList)
//线路单个产品信息接口 lineProduct
app.get('/lvmama/line/product/', lvmama.lineProduct)

//v2版本
//批量景区基本信息接口
app.get('/lvmama/scenicinfolistbypage/',lvmama.scenicInfoListByPage)
//批量产品/商品信息接口
app.get('/lvmama/productinfolistbypage',lvmama.productInfoListByPage)
//批量价格/库存信息接口
app.get('/lvmama/productpricelistbypage',lvmama.productPriceListByPage)
//按 ID 获取产品信息接口
app.get('/lvmama/productinfolist',lvmama.productInfoList)
//按 ID 获取商品信息接口
app.get('/lvmama/goodinfolist',lvmama.goodInfoList)
//按 ID 获取价格库存接口
app.get('/lvamam/goodpricelist',lvmama.goodPriceList)
//按 ID 获取景区信息接口
app.get('/lvamam/scenicinfolist',lvmama.scenicInfoList)
//按商品 ID 查询场次信息接口
app.get('/lvamam/searchseasonticket',lvmama.searchSeasonTicket)


//水煤电
app.get('/bills/wecbill/query',bills.query)

app.get('/bills/wecbill/post',bills.post)


app.listen(app.get('port'), function () {
  console.log('Server started at ' + port)
})